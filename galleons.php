<?php


// Solve this:
// “In Hogwarts the currency is made up of galleon (G) and Sickle (s), and there are seven coins in general circulation:
// 1s, 5s, 10s, 25s, 50s, G1(100s), and G2(200s)
// It's possible to make G3.5 in the following way:
// 1xG2 +1xG1 + 4x10s +1x5s + 5x1s
// How many different ways can G3.5 be made using any number of coins?”



//my code :
function hitungJumlahGalleon($jumlah, $koin) {
    if ($jumlah == 0) {
        return 1;
    }
    
    if ($jumlah < 0 || empty($koin)) {
        return 0;
    }
    
    $koinPertama = $koin[0];
    $sisaKoin = array_slice($koin, 1);
    
    $include = hitungJumlahGalleon($jumlah - $koinPertama, $koin);
    $exclude = hitungJumlahGalleon($jumlah, $sisaKoin);
    
    return $include + $exclude;
}

$koin = [200, 100, 50, 25, 10, 5, 1];

//G3.5 = 3.5 * 100 = 350
$jumlah = 350;

$hasil = hitungJumlahGalleon($jumlah, $koin);

echo "Number of ways to make G3.5: " . $hasil;

?>
